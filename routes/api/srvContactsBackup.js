var EXPRESS=require('express');
var ROUTER=EXPRESS.Router();
var UNDERSCORE=require('underscore');
var F_HANDLE=require('fs');
var DB_PHONEBOOK_PATH='dal/seed.json';
var DB_PHONEBOOK=[];
try{
    DB_PHONEBOOK=JSON.parse(F_HANDLE.readFileSync(DB_PHONEBOOK_PATH));
}catch(ex){
    ;
}


//BACKUP
ROUTER.post('/', function(req, res, next){
    DB_PHONEBOOK.contacts=req.body;
    F_HANDLE.rename(DB_PHONEBOOK_PATH, (DB_PHONEBOOK_PATH.split('.')[0] +'_'+ (new Date()).getTime() +'.json.bak'),
        function(){
            F_HANDLE.writeFileSync(DB_PHONEBOOK_PATH, JSON.stringify(DB_PHONEBOOK));
        });
    res.sendStatus(200);
});


module.exports=ROUTER;


